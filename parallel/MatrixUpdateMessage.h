#ifndef MATRIXUPDATEMESSAGE_H
#define MATRIXUPDATEMESSAGE_H

class MatrixUpdateMessage;

#include "Message.h"
#include "Matrix.h"

class MatrixUpdateMessage : public Message
{
protected:
    Matrix matrix;

    void serializeRest(std::vector<uint8_t> &blob);

    void unserializeRest(std::vector<uint8_t> &blob, uint32_t pos);
public:
    MatrixUpdateMessage(Matrix &matrix);
    MatrixUpdateMessage();

    Matrix &getMatrix();
};

#endif
