#include "MatrixUpdateMessage.h"
#include "Common.h"

void MatrixUpdateMessage::serializeRest(std::vector<uint8_t> &blob)
{
    uint32ToBytes(matrix.getSize(), blob);

    uint8_t **array = matrix.getArray();
    for(uint32_t i = 0; i < matrix.getSize(); i++)
    {
        for(uint32_t j = 0; j < matrix.getSize(); j++)
        {
            uint32ToBytes(array[i][j], blob);
        }
    }
}

void MatrixUpdateMessage::unserializeRest(std::vector<uint8_t> &blob, uint32_t pos)
{
    matrix.initialize(uint32FromBytes(blob, pos));
    pos += 4;
    uint8_t **array = matrix.getArray();
    for(uint32_t i = 0; i < matrix.getSize(); i++)
    {
        for(uint32_t j = 0; j < matrix.getSize(); j++)
        {
            array[i][j] = uint32FromBytes(blob, pos);
            pos += 4;
        }
    }
}

MatrixUpdateMessage::MatrixUpdateMessage(Matrix &matrix)
        : Message(MATRIXUPDATE_MESSAGE), matrix(matrix)
{
}

MatrixUpdateMessage::MatrixUpdateMessage()
        : Message(MATRIXUPDATE_MESSAGE)
{
}

Matrix &MatrixUpdateMessage::getMatrix()
{
    return matrix;
}
