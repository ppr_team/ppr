#include "NoJobAvailableMessage.h"

NoJobAvailableMessage::NoJobAvailableMessage()
        : Message(NOJOBAVAILABLE_MESSAGE)
{

}
NoJobAvailableMessage::~NoJobAvailableMessage()
{
}

void NoJobAvailableMessage::serializeRest(std::vector<uint8_t> &blob)
{
}

void NoJobAvailableMessage::unserializeRest(std::vector<uint8_t> &blob, uint32_t pos)
{
}
