#include "Common.h"

#include "ResultMessage.h"

void ResultMessage::serializeRest(std::vector<uint8_t> &blob)
{
    uint32ToBytes(max, blob);
}

void ResultMessage::unserializeRest(std::vector<uint8_t> &blob, uint32_t pos)
{
    max = uint32FromBytes(blob, pos);
}

ResultMessage::ResultMessage()
    : Message(RESULT_MESSAGE), max(0)
{
}

ResultMessage::ResultMessage(const uint32_t &max)
        : Message(RESULT_MESSAGE), max(max)
{

}

ResultMessage::~ResultMessage()
{
}

uint32_t &ResultMessage::getMax()
{
    return max;
}
