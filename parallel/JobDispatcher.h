#ifndef JOBDISPATCHER_H
#define JOBDISPATCHER_H

class JobDispatcher;

#include <queue>
#include <set>
#include <cstdint>

class JobDispatcher
{
protected:
    uint32_t nextRequest;
    uint32_t lastBegin;
    bool acknowledged;
    bool available;
    bool started;

public:
    JobDispatcher();
    void updateInit();
    uint32_t getNext();
    bool hasStarted();
    void setStarted();
    void resetStarted();
    bool hasCheckedAll();
    void setAcknowledged();
    void resetAcknowledged();
    bool isAcknowledged();
    bool isAvailable();
    void setAvailable(bool available);
    void updateLast();

    uint32_t __get_nextRequest();
    uint32_t __get_lastBegin();
};

#endif

