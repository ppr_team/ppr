#include <iostream>
#include "CommunicationHandler.h"
#include "Common.h"
#include "MPIUtil.h"
#include "RequestMatrixMessage.h"
#include "MatrixUpdateMessage.h"
#include "JobAssignmentMessage.h"
#include "ResultMessage.h"
#include "ExitMessage.h"
#include "JobCheckMessage.h"


CommunicationHandler::CommunicationHandler()
        : state(State::INIT), motherShipShouldSendJobCheck(true), hasJobCheck(false),
          jobCheckColor(COLOR_WHITE), myColor(COLOR_WHITE)
{
}

static const char *stateStrs[] = {
        "INIT",
        "AWAIT_MATRIX",
        "UNEMPLOYED",
        "EMPLOYED",
        "FINISH",
        "NO_JOB",
        "WAIT_FOR_FINISH",
        "WAIT_FOR_EXIT",
        "EXIT"
};

void CommunicationHandler::handle()
{
#ifdef DEBUG
    std::cout << myID() << "handle: " << stateStrs[state] << std::endl;
#endif

#ifndef ASYNCHRONOUS_RECEIVER
    comm.synchronousReceiver.receiveAll();
#endif

    handleReceivedMessages();

    switch(state)
    {
        case INIT:
            handleInit();
            break;
        case AWAIT_MATRIX:
            handleAwaitMatrix();
            break;
        case UNEMPLOYED:
            handleUnemployed();
            break;
        case EMPLOYED:
            handleEmployed();
            break;
        case FINISH:
            handleFinish();
            break;
        case NO_JOB:
            handleNoJob();
            break;
        case WAIT_FOR_FINISH:
            handleWaitForFinish();
            break;
        case WAIT_FOR_EXIT:
            handleWaitForExit();
            break;
        case EXIT:
            handleExit();
            break;
    }
}

void CommunicationHandler::handleReceivedMessages()
{
    std::vector<std::shared_ptr<Message> > delayedMessages;

#ifdef ASYNCHRONOUS_RECEIVER
    while(comm.receiver.hasPayload())
#else
    while(comm.synchronousReceiver.hasPayload())
#endif
    {
#ifdef ASYNCHRONOUS_RECEIVER
        std::shared_ptr<Message> payload = comm.receiver.getPayload();
#else
        std::shared_ptr<Message> payload = comm.synchronousReceiver.getPayload();
#endif
        bool consumed = true;
        switch(payload->type)
        {
            case FINISH_MESSAGE:
                consumed = handleFinishMessage(dynamic_cast<FinishMessage *>(payload.get()));
                break;
            case UNEMPLOYED_MESSAGE:
                consumed = handleUnemployedMessage(dynamic_cast<UnemployedMessage *>(payload.get()));
                break;
            case NOJOBAVAILABLE_MESSAGE:
                consumed = handleNoJobAvailableMessage(dynamic_cast<NoJobAvailableMessage *>(payload.get()));
                break;
            case JOBASSIGNMENT_MESSAGE:
                consumed = handleJobAssignmentMessage(dynamic_cast<JobAssignmentMessage *>(payload.get()));
                break;
            case REQUESTMATRIX_MESSAGE:
                consumed = handleRequestMatrixMessage(dynamic_cast<RequestMatrixMessage *>(payload.get()));
                break;
            case MATRIXUPDATE_MESSAGE:
                consumed = handleMatrixUpdateMessage(dynamic_cast<MatrixUpdateMessage *>(payload.get()));
                break;
            case RESULT_MESSAGE:
                consumed = handleResultMessage(dynamic_cast<ResultMessage *>(payload.get()));
                break;
            case JOBCHECK_MESSAGE:
                consumed = handleJobCheckMessage(dynamic_cast<JobCheckMessage *>(payload.get()));
                break;
            case EXIT_MESSAGE:
                consumed = handleExitMessage(dynamic_cast<ExitMessage *>(payload.get()));
                break;
            default:
                std::cerr << myID() << "handleReceivedMessages: Unknown message type!" << std::endl;
                exit(1);
        }
        if(!consumed)
        {
            delayedMessages.push_back(payload);
        }
    }

    for(auto &payload : delayedMessages)
    {
#ifdef ASYNCHRONOUS_RECEIVER
        comm.receiver.insertBack(payload);
#else
        comm.synchronousReceiver.insertBack(payload);
#endif
    }
}

bool CommunicationHandler::handleFinishMessage(FinishMessage *finishMessage)
{
    state = State::FINISH;
    return true;
}

bool CommunicationHandler::handleUnemployedMessage(UnemployedMessage *unemployedMessage)
{
    if(algo.getStates().size() > 1)
    {
        //JobAssignmentMessage jobAssignmentMessage(algo.getStates().giveTopQuarter().getData());
        JobAssignmentMessage jobAssignmentMessage(algo.getStates().giveInterleaved().getData());
        comm.sender.send(jobAssignmentMessage, unemployedMessage->source);
        if(unemployedMessage->source < MPIUtil::id)
        {
            myColor = COLOR_BLACK;
        }
    }
    else
    {
        if(algo.getStates().size() == 1)
        {
            if(unemployedMessage->source < MPIUtil::id)
            {
                myColor = COLOR_BLACK;
            }
            return false;
        }
        else
        {
            NoJobAvailableMessage noJobAvailableMessage;
            comm.sender.send(noJobAvailableMessage, unemployedMessage->source);
        }
    }
    return true;
}

bool CommunicationHandler::handleNoJobAvailableMessage(NoJobAvailableMessage *noJobAvailableMessage)
{
    jobDispatcher.setAvailable(false);
    jobDispatcher.setAcknowledged();
    return true;
}

bool CommunicationHandler::handleJobAssignmentMessage(JobAssignmentMessage *jobAssignmentMessage)
{
    jobDispatcher.setAvailable(true);
    jobDispatcher.setAcknowledged();

    algo.getStates().assignData(jobAssignmentMessage->getStack());
    return true;
}

bool CommunicationHandler::handleRequestMatrixMessage(RequestMatrixMessage *requestMatrixMessage)
{
    if(MPIUtil::id == MOTHERSHIP)
    {
        MatrixUpdateMessage matrixUpdateMessage(matrix);
#ifdef DEBUG
        std::cout << myID() << "Sending matrix to: " << requestMatrixMessage->source << std::endl;
#endif
        comm.sender.send(matrixUpdateMessage, requestMatrixMessage->source);
    }
    else
    {
        std::cerr << myID() << "handleRequestMatrixMessage: Not MOTHERSHIP!" << std::endl;
        exit(1);
    }
    return true;
}

bool CommunicationHandler::handleMatrixUpdateMessage(MatrixUpdateMessage *matrixUpdateMessage)
{
    matrix = matrixUpdateMessage->getMatrix();
    state = State::UNEMPLOYED;
#ifdef DEBUG
    std::cout << myID() << "handleMatrixUpdateMessage: Updated matrix." << std::endl;
#endif
    return true;
}

bool CommunicationHandler::handleResultMessage(ResultMessage *resultMessage)
{
    if(MPIUtil::id != MOTHERSHIP)
    {
        std::cerr << myID() << "handleResultMessage: Not MOTHERSHIP!" << std::endl;
        exit(1);
    }
    if(resultMessage->getMax() > algo.getBestMax())
    {
        algo.setBestMax(resultMessage->getMax());
    }
    std::cout << myID() << "handleResultMessage: Got max: " << resultMessage->getMax() << std::endl;

    resultCheckTable.postResult(resultMessage->source, false);
    if(resultCheckTable.allResponded())
    {
        std::cout << myID() << "All responded!"<< std::endl;
        ExitMessage exitMessage;
        std::cout << myID() << "will send exit!"<< std::endl;
        comm.sender.sendAll(exitMessage, true);
        std::cout << myID() << "Exit sent!"<< std::endl;
        state = State::EXIT;
    }
    return true;
}

bool CommunicationHandler::handleJobCheckMessage(JobCheckMessage *jobCheckMessage)
{
    std::cout << myID() << "handleJobCheckMessage: color: " << (int) jobCheckMessage->getColor() << std::endl;
    if(MPIUtil::id == MOTHERSHIP)
    {
        if(jobCheckMessage->getColor() == COLOR_WHITE)
        {
            FinishMessage finishMessage;
            comm.sender.sendAll(finishMessage, true);
            resultCheckTable.clear();
            state = State::WAIT_FOR_EXIT;
        }
        else
        {
            motherShipShouldSendJobCheck = true;
        }
    }
    else
    {
        hasJobCheck = true;
        jobCheckColor = jobCheckMessage->getColor();
    }
    return true;
}

bool CommunicationHandler::handleExitMessage(ExitMessage *exitMessage)
{
    state = State::EXIT;
    return true;
}

void CommunicationHandler::handleInit()
{
    if(MPIUtil::id == MOTHERSHIP)
    {
        state = State::EMPLOYED;
        return;
    }
    state = State::AWAIT_MATRIX;
    RequestMatrixMessage requestMatrixMessage;
    comm.sender.send(requestMatrixMessage, MOTHERSHIP);
}

void CommunicationHandler::handleAwaitMatrix()
{
#ifdef ASYNCHRONOUS_RECEIVER
    comm.receiver.waitForPayload();
#else
    comm.synchronousReceiver.waitUntilReceived();
#endif
}

void CommunicationHandler::handleUnemployed()
{
    if(jobDispatcher.isAcknowledged() || !jobDispatcher.hasStarted())
    {
        jobDispatcher.resetAcknowledged();

        if(jobDispatcher.isAvailable())
        {
            state = State::EMPLOYED;
            jobDispatcher.resetStarted();
            jobDispatcher.updateLast();
            jobDispatcher.setAvailable(false);

#ifdef DEBUG
            std::cout << myID() << ">>>>>>>>>> =========== > Assigned new job, RESETING. next: " << jobDispatcher.__get_nextRequest() << " last: " << jobDispatcher.__get_lastBegin() << std::endl;
#endif
        }
        else
        {
#ifdef DEBUG
            std::cout << myID() << ">>>>>>>>>> =========== > Handle no job. next: " << jobDispatcher.__get_nextRequest() << " last: " << jobDispatcher.__get_lastBegin() << std::endl;
#endif

            if(jobDispatcher.hasCheckedAll())
            {
                jobDispatcher.resetStarted();
                jobDispatcher.getNext();
#ifdef DEBUG
                std::cout << myID() << "No job available.." << std::endl;
#endif
                state = State::NO_JOB;
            }
            else
            {
                jobDispatcher.setStarted();
                uint32_t next = jobDispatcher.getNext();
                if(next == (uint32_t) MPIUtil::id)
                {
                    jobDispatcher.setAcknowledged();
                    jobDispatcher.setAvailable(false);
                }
                else
                {
                    UnemployedMessage unemployedMessage;
#ifdef DEBUG
                    std::cout << myID() << "Trying to get job from: " << next << std::endl;
#endif
                    comm.sender.send(unemployedMessage, next);
                }
            }
        }
    }
    else
    {
#ifdef ASYNCHRONOUS_RECEIVER
        comm.receiver.waitForPayload();
#else
        comm.synchronousReceiver.waitUntilReceived();
#endif
    }
}

void CommunicationHandler::handleEmployed()
{
    if(algo.getStates().empty())
    {
#ifdef DEBUG
        std::cout << myID() << "# Job done. Current max: " << algo.getBestMax() << std::endl;
#endif
        state = State::UNEMPLOYED;
    }
    else
    {
        algo.compute();
    }
}

void CommunicationHandler::handleFinish()
{
    ResultMessage resultMessage(algo.getBestMax());
    comm.sender.send(resultMessage, MOTHERSHIP);

    state = State::WAIT_FOR_EXIT;
}

void CommunicationHandler::handleNoJob()
{
    if(MPIUtil::id == MOTHERSHIP && motherShipShouldSendJobCheck)
    {
        motherShipShouldSendJobCheck = false;

        JobCheckMessage jobCheckMessage(COLOR_WHITE);
        comm.sender.send(jobCheckMessage, (MPIUtil::id + 1) % MPIUtil::cpuCount);
    }
    else
    {
        if(hasJobCheck)
        {
            hasJobCheck = false;
            if(myColor == COLOR_BLACK)
            {
                jobCheckColor = COLOR_BLACK;
                myColor = COLOR_WHITE;
            }
            JobCheckMessage checkMessage(jobCheckColor);
            comm.sender.send(checkMessage, (MPIUtil::id + 1) % MPIUtil::cpuCount);

            std::chrono::milliseconds dura(20);
            std::this_thread::sleep_for(dura);
        }
        else
        {
            std::chrono::milliseconds dura(10);
            std::this_thread::sleep_for(dura);
        }
    }

    state = State::UNEMPLOYED;
}

void CommunicationHandler::handleWaitForFinish()
{
#ifdef ASYNCHRONOUS_RECEIVER
    comm.receiver.waitForPayload();
#else
    comm.synchronousReceiver.waitUntilReceived();
#endif
}

void CommunicationHandler::handleWaitForExit()
{
#ifdef DEBUG
    std::cout << myID() << "Waiting for exit." << std::endl;
#endif
#ifdef ASYNCHRONOUS_RECEIVER
    comm.receiver.waitForPayload();
#else
    comm.synchronousReceiver.waitUntilReceived();
#endif
}

bool CommunicationHandler::shouldExit()
{
    return state == State::EXIT;
}

void CommunicationHandler::handleExit()
{

}
