#ifndef COMMON_H
#define COMMON_H

//#define ASYNCHRONOUS_RECEIVER
#define ASYNCHRONOUS_SENDER
//#define DEBUG
//#define DEBUG_MESSAGES

#include <string>
#include <cstdint>
#include <vector>
#include "Algorithm.h"
#include "Comm.h"
#include "JobDispatcher.h"
#include "CommunicationHandler.h"
#include "ResultCheckTable.h"

#define MOTHERSHIP 0

extern Algorithm algo;
extern Comm comm;
extern CommunicationHandler communicationHandler;
extern JobDispatcher jobDispatcher;
extern Matrix matrix;
extern ResultCheckTable resultCheckTable;

std::string myID();

void int32ToBytes(int32_t val, std::vector<uint8_t> &bytes);
void uint32ToBytes(uint32_t val, std::vector<uint8_t> &bytes);

int32_t int32FromBytes(std::vector<uint8_t> &bytes, size_t pos);
uint32_t uint32FromBytes(std::vector<uint8_t> &bytes, size_t pos);

int64_t currentTimeMilis();

#endif
