#ifndef REQUESTMATRIXMESSAGE_H
#define REQUESTMATRIXMESSAGE_H

class RequestMatrixMessage;

#include "Message.h"

class RequestMatrixMessage : public Message
{
public:
    RequestMatrixMessage();
    virtual ~RequestMatrixMessage();
protected:
    virtual void serializeRest(std::vector<uint8_t> &blob);
    virtual void unserializeRest(std::vector<uint8_t> &blob, uint32_t pos);
};

#endif
