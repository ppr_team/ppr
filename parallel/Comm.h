#include <thread>

#ifndef COMM_H
#define COMM_H

class Comm;

#include <vector>
#include <memory>
#include <queue>
#include <condition_variable>
#include <mutex>
#include "Message.h"

#define COMM_RECV_BUFFER_SIZE (1024 * 1024 * 16)

class Comm
{
protected:
    std::condition_variable conditionVariable;
    std::mutex mutex;
public:
    class ThreadWorker
    {
    protected:
        std::thread thread;
        bool running;
        virtual void threadFunc() = 0;
        std::mutex mutex;
        std::condition_variable conditionVariable;

    public:
        virtual ~ThreadWorker();
        void start();
        void stop();
        bool isRunning();
    };

    class Sender : public ThreadWorker
    {
    protected:
        struct Payload
        {
            std::vector<uint8_t> data;
        };
        virtual void threadFunc();
        std::queue<std::shared_ptr<Payload> > payloads;
        std::queue<uint32_t> targets;
    public:
        void send(Message &message, uint32_t target);
        void sendAll(Message &message, bool myself);
    };

    class Receiver : public ThreadWorker
    {
    protected:
        virtual void threadFunc();
        uint8_t *recvBuffer;
        std::queue<std::shared_ptr<Message> > payloads;
    public:
        Receiver();
        virtual ~Receiver();
        bool hasPayload();
        void waitForPayload();
        void insertBack(std::shared_ptr<Message> &payload);
        std::shared_ptr<Message> getPayload();
    };

    class SynchronousReceiver
    {
    protected:
        std::queue<std::shared_ptr<Message> > payloads;
        uint8_t *recvBuffer;
        bool running;
    public:
        SynchronousReceiver();
        ~SynchronousReceiver();
        void insertBack(std::shared_ptr<Message> &payload);
        void receiveAll();
        void waitUntilReceived();
        bool hasPayload();
        void setRunning(bool run);
        bool isRunning();
        std::shared_ptr<Message> getPayload();
    };

public:
    void initialize();
    void finalize();

    static Sender sender;
    // static Receiver receiver;
    static SynchronousReceiver synchronousReceiver;
};

#endif
