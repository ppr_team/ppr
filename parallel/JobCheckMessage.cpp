#include "JobCheckMessage.h"

void JobCheckMessage::serializeRest(std::vector<uint8_t> &blob)
{
    blob.push_back(color);
}

void JobCheckMessage::unserializeRest(std::vector<uint8_t> &blob, uint32_t pos)
{
    color = blob[pos];
}

JobCheckMessage::JobCheckMessage()
    : Message(JOBCHECK_MESSAGE), color(COLOR_WHITE)
{

}

JobCheckMessage::JobCheckMessage(const uint8_t &color)
    : Message(JOBCHECK_MESSAGE), color(color)
{

}

JobCheckMessage::~JobCheckMessage()
{
}

uint8_t &JobCheckMessage::getColor()
{
    return color;
}
