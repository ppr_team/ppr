#include "Comm.h"
#include "Common.h"
#include "MPIUtil.h"
#include "Message.h"
#include "FinishMessage.h"
#include <iostream>

void Comm::ThreadWorker::start()
{
    running = true;
    thread = std::thread(&Comm::ThreadWorker::threadFunc, this);
}

void Comm::ThreadWorker::stop()
{
    running = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        conditionVariable.notify_all();
    }
    if(thread.joinable())
    {
        thread.join();
    }
}

Comm::ThreadWorker::~ThreadWorker()
{
}

bool Comm::ThreadWorker::isRunning()
{
    return running;
}

void Comm::Sender::send(Message &message, uint32_t target)
{
    std::shared_ptr<Payload> payload(new Payload);

#ifdef DEBUG
    std::cout << myID() << "Sending message: " << messageStrs[message.type] << " to: " << target << std::endl;
#endif

    message.serialize(payload->data);

    std::unique_lock<std::mutex> lock(mutex);

#ifdef ASYNCHRONOUS_SENDER
    payloads.push(payload);
    targets.push(target);
#else
    MPIUtil::sendBlocking(payload->data.data(), (uint32_t) payload->data.size(), target);
#endif
    conditionVariable.notify_one();
}

void Comm::Sender::sendAll(Message &message, bool myself)
{
    std::shared_ptr<Payload> payload(new Payload);

#if defined(DEBUG) || defined(DEBUG_MESSAGES)
    std::cout << myID() << "Sending message to All: typeid: " << (int) message.type << " typeStr: " << messageStrs[message.type] << " myself?: " << myself << std::endl;
#endif

    message.serialize(payload->data);

#ifdef ASYNCHRONOUS_SENDER
    std::unique_lock<std::mutex> lock(mutex);
#endif

    for(uint32_t i = 0; i < (uint32_t) MPIUtil::cpuCount; i++)
    {
        if(i != (uint32_t) MPIUtil::id || myself)
        {
#ifdef ASYNCHRONOUS_SENDER
            payloads.push(payload);
            targets.push(i);
#else
            MPIUtil::sendBlocking(payload->data.data(), (uint32_t) payload->data.size(), i);
#endif
        }
    }
#ifdef ASYNCHRONOUS_SENDER
    conditionVariable.notify_one();
#endif
}

void Comm::Sender::threadFunc()
{
#ifdef DEBUG
    std::cout << myID() << "Sender thread starting!" << std::endl;
#endif

    {
        std::unique_lock<std::mutex> lock(comm.mutex);
        comm.conditionVariable.notify_one();
    }
    while(running || payloads.size() > 0)
    {
#ifdef DEBUG
        std::cout << myID() << "----> Sender thread loop!" << std::endl;
#endif
        std::unique_lock<std::mutex> lock(mutex);
        if (payloads.size() > 0)
        {
            std::vector<uint8_t> payload = payloads.front()->data;
            payloads.pop();

            uint32_t target = targets.front();
            targets.pop();

            MPIUtil::sendBlocking(payload.data(), (uint32_t) payload.size(), target);
        }
        else
        {
            conditionVariable.wait(lock);
        }
    }
#ifdef DEBUG
    std::cout << myID() << "Sender thread stopped!" << std::endl;
#endif
}

bool  Comm::Receiver::hasPayload()
{
    return !payloads.empty();
}

std::shared_ptr<Message> Comm::Receiver::getPayload()
{
    std::shared_ptr<Message> res = payloads.front();
    payloads.pop();
    return res;
}

void Comm::Receiver::waitForPayload()
{
    std::unique_lock<std::mutex> lock(mutex);

    if(payloads.size() > 0)
        return;

    conditionVariable.wait(lock);
}

void Comm::Receiver::threadFunc()
{
#ifdef DEBUG
    std::cout << myID() << "Receiver thread starting!" << std::endl;
#endif
    {
        std::unique_lock<std::mutex> lock(comm.mutex);
        comm.conditionVariable.notify_one();
    }

    while(running)
    {
#ifdef DEBUG
        std::cout << myID() << "----> Receiver thread loop!" << std::endl;
#endif
        uint32_t size = MPIUtil::receiveBlocking(recvBuffer, COMM_RECV_BUFFER_SIZE);
#ifdef DEBUG
        std::cout << myID() << "size: " << size << " running: " << running << std::endl;
#endif
        if(!running)
        {
            std::unique_lock<std::mutex> lock(mutex);
            conditionVariable.notify_one();
            return;
        }
#ifdef DEBUG
        std::cout << myID() << "---- +> Receiver got message of length: " << size << std::endl;
#endif
        std::vector<uint8_t> payload(recvBuffer, recvBuffer + size);

        std::unique_lock<std::mutex> lock(mutex);

        std::shared_ptr<Message> message = Message::unserializeMessage(payload);
        payloads.push(message);
        conditionVariable.notify_one();
    }
#ifdef DEBUG
    std::cout << myID() << "Receiver thread stopped!" << std::endl;
#endif
}

void Comm::Receiver::insertBack(std::shared_ptr<Message> &payload)
{
    std::unique_lock<std::mutex> lock(mutex);
    payloads.push(payload);
    conditionVariable.notify_one();
}

Comm::Receiver::Receiver()
    : recvBuffer(new uint8_t[COMM_RECV_BUFFER_SIZE])
{

}

Comm::Receiver::~Receiver()
{
    delete [] recvBuffer;
}

Comm::SynchronousReceiver::SynchronousReceiver()
    : running(true)
{
    recvBuffer = new uint8_t[COMM_RECV_BUFFER_SIZE];
}

Comm::SynchronousReceiver::~SynchronousReceiver()
{
    delete [] recvBuffer;
}


void Comm::SynchronousReceiver::insertBack(std::shared_ptr<Message> &payload)
{
    payloads.push(payload);
}

void Comm::SynchronousReceiver::receiveAll()
{
    while(MPIUtil::checkIncomming())
    {
        uint32_t size = MPIUtil::receiveImmediate(recvBuffer, COMM_RECV_BUFFER_SIZE);
        std::vector<uint8_t> payload(recvBuffer, recvBuffer + size);
        std::shared_ptr<Message> message = Message::unserializeMessage(payload);
        payloads.push(message);
    }
}

void Comm::SynchronousReceiver::waitUntilReceived()
{
    if(hasPayload())
    {
        return;
    }
    uint32_t size = MPIUtil::receiveBlocking(recvBuffer, COMM_RECV_BUFFER_SIZE);
    std::vector<uint8_t> payload(recvBuffer, recvBuffer + size);
    std::shared_ptr<Message> message = Message::unserializeMessage(payload);
    payloads.push(message);
}

bool Comm::SynchronousReceiver::hasPayload()
{
    return !payloads.empty();
}

std::shared_ptr<Message> Comm::SynchronousReceiver::getPayload()
{
    std::shared_ptr<Message> res = payloads.front();
    payloads.pop();
    return res;
}

void Comm::SynchronousReceiver::setRunning(bool run)
{
    this->running = run;
}

bool Comm::SynchronousReceiver::isRunning()
{
    return running;
}

void Comm::initialize()
{
    /*{
        std::unique_lock<std::mutex> lock(mutex);
        receiver.start();
        conditionVariable.wait(lock);
    }*/
    {
        std::unique_lock<std::mutex> lock(mutex);
#ifdef ASYNCHRONOUS_SENDER
        sender.start();
        conditionVariable.wait(lock);
#endif
    }
#ifdef DEBUG
    std::cout << myID() << "[*] all threads started" << std::endl;
#endif
}

void Comm::finalize()
{
#ifdef ASYNCHRONOUS_RECEIVER
    receiver.stop();
#else
    synchronousReceiver.setRunning(false);
#endif

#ifdef ASYNCHRONOUS_SENDER
    sender.stop();
#endif
}

#ifdef ASYNCHRONOUS_RECEIVER
Comm::Receiver Comm::receiver;
#else
Comm::SynchronousReceiver Comm::synchronousReceiver;
#endif

Comm::Sender Comm::sender;
