#ifndef EXITMESSAGE_H
#define EXITMESSAGE_H

class ExitMessage;

#include "Message.h"

class ExitMessage : public Message
{
protected:
    virtual void serializeRest(std::vector<uint8_t> &blob);
    virtual void unserializeRest(std::vector<uint8_t> &blob, uint32_t pos);
public:
    ExitMessage();
    ~ExitMessage();
};

#endif
