#include "DummyMessage.h"

DummyMessage::DummyMessage()
    : Message(DUMMY_MESSAGE)
{
}

DummyMessage::~DummyMessage()
{
}

void DummyMessage::serializeRest(std::vector<uint8_t> &blob)
{
    // dummy
}

void DummyMessage::unserializeRest(std::vector<uint8_t> &blob, uint32_t pos)
{
    // dummy
}
