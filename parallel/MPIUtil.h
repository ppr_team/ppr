#ifndef MPIUTIL_H
#define MPIUTIL_H

#include <cstdint>

namespace MPIUtil
{
    extern int32_t cpuCount;
    extern int32_t id;

    void initialize(int *argc, char ***argv);
    void finalize();

    void sendBlocking(void *buffer, uint32_t size, uint32_t dest);
    uint32_t receiveBlocking(void *buffer, uint32_t size);
    uint32_t receiveImmediate(void *buffer, uint32_t size);
    bool checkIncomming();
}

#endif
