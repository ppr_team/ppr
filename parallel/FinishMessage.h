#ifndef FINISHMESSAGE_H
#define FINISHMESSAGE_H

#include "Message.h"

class FinishMessage : public Message
{
public:
    FinishMessage();
    virtual ~FinishMessage();

    virtual void serializeRest(std::vector<uint8_t> &blob);
    virtual void unserializeRest(std::vector<uint8_t> &blob, uint32_t pos);
};

#endif
