#ifndef NOJOBAVAILABLEMESSAGE_H
#define NOJOBAVAILABLEMESSAGE_H

class NoJobAvailableMessaeg;

#include "Message.h"

class NoJobAvailableMessage : public Message
{
public:
    NoJobAvailableMessage();
    virtual ~NoJobAvailableMessage();

protected:
    virtual void serializeRest(std::vector<uint8_t> &blob);
    virtual void unserializeRest(std::vector<uint8_t> &blob, uint32_t pos);
};

#endif
