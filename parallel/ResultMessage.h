#ifndef RESULTMESSAGE_H
#define RESULTMESSAGE_H

class ResultMessage;

#include "Message.h"

class ResultMessage : public Message
{
protected:
    uint32_t max;
    virtual void serializeRest(std::vector<uint8_t> &blob);
    virtual void unserializeRest(std::vector<uint8_t> &blob, uint32_t pos);
public:
    ResultMessage();
    ResultMessage(const uint32_t &max);
    virtual ~ResultMessage();
    uint32_t &getMax();
};

#endif
