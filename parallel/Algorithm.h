#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "Matrix.h"
#include "Parallelstack.h"

/**
* The computing algorithm.
*
* This might as well be an abstract, although the project will
* benefit if it is split into 2 projects.
*/
class Algorithm
{
protected:
    ParallelStack<std::vector<uint32_t> > states;
    uint32_t bestMax;
public:
    Algorithm();
    void compute();
    ParallelStack<std::vector<uint32_t> > &getStates();
    uint32_t getBestMax();
    void setBestMax(uint32_t max);
};

#endif
