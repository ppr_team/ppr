#include "Common.h"
#include "MPIUtil.h"
#include <chrono>

std::string myID()
{
    return std::string("[") + std::to_string(MPIUtil::id) + std::string("]: ");
}

void int32ToBytes(int32_t val, std::vector<uint8_t> &bytes)
{
    uint32_t *sign = (uint32_t *) &val;
    uint32ToBytes(*sign, bytes);
}

void uint32ToBytes(uint32_t val, std::vector<uint8_t> &bytes)
{
    uint8_t byteArr[4];
    uint8_t *valBs = (uint8_t *) &val;
    byteArr[0] = valBs[0];
    byteArr[1] = valBs[1];
    byteArr[2] = valBs[2];
    byteArr[3] = valBs[3];

    bytes.insert(bytes.end(), std::begin(byteArr), std::end(byteArr));
}

int32_t int32FromBytes(std::vector<uint8_t> &bytes, size_t pos)
{
    uint32_t val = uint32FromBytes(bytes, pos);
    return *(int32_t *) &val;
}

uint32_t uint32FromBytes(std::vector<uint8_t> &bytes, size_t pos)
{
    uint32_t val;
    uint8_t *valBs = (uint8_t *) &val;

    valBs[0] = bytes[pos];
    valBs[1] = bytes[pos + 1];
    valBs[2] = bytes[pos + 2];
    valBs[3] = bytes[pos + 3];

    return val;
}

int64_t currentTimeMilis()
{
    namespace sc = std::chrono;

    auto time = sc::system_clock::now();
    auto since_epoch = time.time_since_epoch();
    auto millis = sc::duration_cast<sc::milliseconds>(since_epoch);

    return (int64_t) millis.count();
}

Algorithm algo;
Comm comm;
CommunicationHandler communicationHandler;
ResultCheckTable resultCheckTable;
JobDispatcher jobDispatcher;
Matrix matrix;
