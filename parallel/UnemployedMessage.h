#ifndef UNEMPLOYEDMESSAGE_H
#define UNEMPLOYEDMESSAGE_H

class UnemployedMessage;

#include "Message.h"

class UnemployedMessage : public Message
{
protected:
    virtual void serializeRest(std::vector<uint8_t> &blob);
    virtual void unserializeRest(std::vector<uint8_t> &blob, uint32_t pos);
public:
    UnemployedMessage();
    virtual ~UnemployedMessage();
};

#endif