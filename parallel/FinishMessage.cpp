#include "FinishMessage.h"

FinishMessage::FinishMessage()
        : Message(FINISH_MESSAGE)
{
}

FinishMessage::~FinishMessage()
{

}

void FinishMessage::serializeRest(std::vector<uint8_t> &blob)
{
}

void FinishMessage::unserializeRest(std::vector<uint8_t> &blob, uint32_t pos)
{
}
