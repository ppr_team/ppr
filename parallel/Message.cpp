#include "Message.h"
#include "Common.h"
#include "DummyMessage.h"
#include "FinishMessage.h"
#include "NoJobAvailableMessage.h"
#include "JobAssignmentMessage.h"
#include "UnemployedMessage.h"
#include "RequestMatrixMessage.h"
#include "MatrixUpdateMessage.h"
#include "ResultMessage.h"
#include "JobCheckMessage.h"
#include "ExitMessage.h"
#include "MPIUtil.h"
#include <iostream>

Message::Message(uint8_t type)
{
    this->type = type;
    this->source = MPIUtil::id;
}

Message::~Message()
{

}

void Message::serialize(std::vector<uint8_t> &blob)
{
    int32ToBytes(source, blob);
    uint32ToBytes(length, blob);
    blob.push_back(type);

    serializeRest(blob);
}

void Message::unserialize(std::vector<uint8_t> &blob)
{
    source = int32FromBytes(blob, 0);
    length = uint32FromBytes(blob, 4);
    type = blob[8];

    unserializeRest(blob, 9);
}

const char *messageStrs[10] = {
        "DummyMessage",
        "FinishMessage",
        "UnemployedMessage",
        "NoJobAvailableMessage",
        "JobAssignmentMessage",
        "RequestMatrixMessage",
        "MatrixUpdateMessage",
        "ResultMessage",
        "JobCheckMessage",
        "ExitMessage"
};

std::shared_ptr<Message> Message::unserializeMessage(std::vector<uint8_t> &blob)
{
    DummyMessage dummyMessage;
    dummyMessage.unserialize(blob);

    std::shared_ptr<Message> targetMessage;

    switch(dummyMessage.type)
    {
        case DUMMY_MESSAGE:
            std::cerr << "Error: No message can be dummy message!" << std::endl;
            exit(1);
            break;
        case FINISH_MESSAGE:
            targetMessage = std::shared_ptr<Message>(new FinishMessage());
            break;
        case UNEMPLOYED_MESSAGE:
            targetMessage = std::shared_ptr<Message>(new UnemployedMessage());
            break;
        case NOJOBAVAILABLE_MESSAGE:
            targetMessage = std::shared_ptr<Message>(new NoJobAvailableMessage());
            break;
        case JOBASSIGNMENT_MESSAGE:
            targetMessage = std::shared_ptr<Message>(new JobAssignmentMessage());
            break;
        case REQUESTMATRIX_MESSAGE:
            targetMessage = std::shared_ptr<Message>(new RequestMatrixMessage());
            break;
        case MATRIXUPDATE_MESSAGE:
            targetMessage = std::shared_ptr<Message>(new MatrixUpdateMessage());
            break;
        case RESULT_MESSAGE:
            targetMessage = std::shared_ptr<Message>(new ResultMessage());
            break;
        case JOBCHECK_MESSAGE:
            targetMessage = std::shared_ptr<Message>(new JobCheckMessage());
            break;
        case EXIT_MESSAGE:
            targetMessage = std::shared_ptr<Message>(new ExitMessage());
            break;
        default:
            std::cerr << "Error: Invalid message type: " << dummyMessage.type << std::endl;
            exit(1);
    }

    if(targetMessage)
    {
#if defined(DEBUG) || defined(DEBUG_MESSAGES)
        std::cout << myID() << "Deserializing message: " << messageStrs[targetMessage->type] << std::endl;
#endif
        targetMessage->unserialize(blob);
    }

    return targetMessage;
}
