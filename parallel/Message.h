#ifndef MESSAGE_H
#define MESSAGE_H

class Message;

#include <memory>
#include <vector>

#define DUMMY_MESSAGE 0
#define FINISH_MESSAGE 1
#define UNEMPLOYED_MESSAGE 2
#define NOJOBAVAILABLE_MESSAGE 3
#define JOBASSIGNMENT_MESSAGE 4
#define REQUESTMATRIX_MESSAGE 5
#define MATRIXUPDATE_MESSAGE 6
#define RESULT_MESSAGE 7
#define JOBCHECK_MESSAGE 8
#define EXIT_MESSAGE 9

extern const char *messageStrs[];

class Message
{
public:
    Message(uint8_t type);
    virtual ~Message();

    int32_t source;
    uint32_t length;
    uint8_t type;

    void serialize(std::vector<uint8_t> &blob);
    void unserialize(std::vector<uint8_t> &blob);
    static std::shared_ptr<Message> unserializeMessage(std::vector<uint8_t> &blob);

    virtual void serializeRest(std::vector<uint8_t> &blob) = 0;
    virtual void unserializeRest(std::vector<uint8_t> &blob, uint32_t pos) = 0;
};

#endif
