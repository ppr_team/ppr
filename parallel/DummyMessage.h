#ifndef DUMMYMESSAGE_H
#define DUMMYMESSAGE_H

class DummyMessage;

#include "Message.h"

class DummyMessage : public Message
{
public:
    DummyMessage();
    virtual ~DummyMessage();

    virtual void serializeRest(std::vector<uint8_t> &blob);
    virtual void unserializeRest(std::vector<uint8_t> &blob, uint32_t pos);
};

#endif
