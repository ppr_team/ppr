#include <iostream>
#include <fstream>
#include <chrono>
#include "Algorithm.h"
#include "MPIUtil.h"
#include "Common.h"
#include "DummyMessage.h"
#include "FinishMessage.h"

using namespace std;

void loadInitialWork(int argc, char **argv)
{
    size_t vertexCount = 0;
    bool fromFile = false;

    std::ifstream *ifs = new std::ifstream;

    if (argc == 2)
    {
        fromFile = true;
        ifs->open(argv[1]);

        *ifs >> vertexCount;
    }
    else
    {
        if (argc > 2)
        {
            std::cerr << myID() << "Invalid amount of arguments." << std::endl;
        }
        else
        {
            std::cin >> vertexCount;
        }
    }

    matrix.initialize(vertexCount);

    if (fromFile)
    {
        matrix.fromStream(*ifs);
        ifs->close();
    }
    else
    {
        matrix.fromStream(std::cin);
    }

    delete ifs;

    if(MPIUtil::id == MOTHERSHIP)
    {
        //init stack
        for (uint32_t i = 0; i < matrix.getSize(); i++)
        {
            std::vector<uint32_t> nodes;
            nodes.push_back(i);
            algo.getStates().push(nodes);

            nodes.clear();
        }
    }

    std::cout << myID() << "Loaded initial job." << std::endl;
}

int main(int argc, char **argv)
{
    MPIUtil::initialize(&argc, &argv);
    comm.initialize();

    jobDispatcher.updateInit();
    resultCheckTable.init();

    int64_t startTime = currentTimeMilis();

    if(MPIUtil::id == MOTHERSHIP)
    {
        std::cout << myID() << "===== THE MOTHERSHIP HAS ARRIVED =====" << std::endl;
        loadInitialWork(argc, argv);
    }
    else
    {
        std::cout << myID() << "===== The dumb slave ship is here. =====" << std::endl;
    }
    while(!communicationHandler.shouldExit())
    {
        communicationHandler.handle();
    }
    communicationHandler.handle();

    if(MPIUtil::id == MOTHERSHIP)
    {
        std::cout << myID() << "Time spent: " << (currentTimeMilis() - startTime) << " miliseconds." << std::endl;
        std::cout << myID() << "The discovered max is: " << algo.getBestMax() << std::endl;
    }

    comm.finalize();
    MPIUtil::finalize();

    std::cout << myID() << "All stopped. Exiting.." << std::endl;

    return 0;
}
