#ifndef JOBASSIGNMENTMESSAGE_H
#define JOBASSIGNMENTMESSAGE_H

class JobAssignmentMessage;

#include "Message.h"
#include <vector>

class JobAssignmentMessage : public Message
{
protected:
    std::vector<std::vector<uint32_t> > stack;
    virtual void serializeRest(std::vector<uint8_t> &blob);
    virtual void unserializeRest(std::vector<uint8_t> &blob, uint32_t pos);
public:
    JobAssignmentMessage();
    JobAssignmentMessage(std::vector<std::vector<uint32_t> > stack);
    virtual ~JobAssignmentMessage();

    std::vector<std::vector<uint32_t> > &getStack();
};

#endif
