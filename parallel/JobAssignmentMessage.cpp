#include "JobAssignmentMessage.h"
#include "Common.h"

void JobAssignmentMessage::serializeRest(std::vector<uint8_t> &blob)
{
    uint32ToBytes(stack.size(), blob);
    for(auto &vectorItem : stack)
    {
        uint32ToBytes(vectorItem.size(), blob);
        for(uint32_t &num : vectorItem)
        {
            uint32ToBytes(num, blob);
        }
    }
}

void JobAssignmentMessage::unserializeRest(std::vector<uint8_t> &blob, uint32_t pos)
{
    stack.clear();
    uint32_t stackSize = uint32FromBytes(blob, pos);
    pos += 4;
    stack.resize(stackSize);
    for(auto &vectorItem : stack)
    {
        uint32_t vectorSize = uint32FromBytes(blob, pos);
        vectorItem.resize(vectorSize);
        pos += 4;
        for(uint32_t &num : vectorItem)
        {
            num = uint32FromBytes(blob, pos);
            pos += 4;
        }
    }
}

JobAssignmentMessage::JobAssignmentMessage()
    : Message(JOBASSIGNMENT_MESSAGE)
{
}

JobAssignmentMessage::JobAssignmentMessage(std::vector<std::vector<uint32_t> > stack)
    : Message(JOBASSIGNMENT_MESSAGE), stack(stack)
{
}

JobAssignmentMessage::~JobAssignmentMessage()
{
}

std::vector<std::vector<uint32_t> > &JobAssignmentMessage::getStack()
{
    return stack;
}
