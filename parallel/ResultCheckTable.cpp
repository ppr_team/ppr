#include <stdio.h>
#include "ResultCheckTable.h"
#include "MPIUtil.h"

ResultCheckTable::ResultCheckTable()
        : checked(NULL)
{
}

ResultCheckTable::~ResultCheckTable()
{
    delete [] checked;
}

void ResultCheckTable::init()
{
    if(!checked)
    {
        checked = new uint8_t[MPIUtil::cpuCount];
    }
    clear();
}

void ResultCheckTable::clear()
{
    for(uint32_t i = 0; i < (uint32_t) MPIUtil::cpuCount; i++)
    {
        checked[i] = NO_RESPONSE;
    }
    checked[MPIUtil::id] = NO_JOB;
}

void ResultCheckTable::postResult(uint32_t id, bool hasJob)
{
    checked[id] = hasJob ? HAS_JOB : NO_JOB;
}

bool ResultCheckTable::allResponded()
{
    for(uint32_t i = 0; i < (uint32_t)MPIUtil::cpuCount; i++)
    {
        if(checked[i] == NO_RESPONSE)
        {
            return false;
        }
    }

    return true;
}

bool ResultCheckTable::isEnd()
{
    for(uint32_t i = 0; i < (uint32_t)MPIUtil::cpuCount; i++)
    {
        if(checked[i] != NO_JOB)
        {
            return false;
        }
    }
    return true;
}
