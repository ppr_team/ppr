#include "Algorithm.h"
#include "Common.h"
#include "Parallelstack.h"
#include "MPIUtil.h"
#include <iostream>
#include <algorithm>

Algorithm::Algorithm()
    : bestMax(0)
{
}

ParallelStack<std::vector<uint32_t> > &Algorithm::getStates()
{
    return states;
}

uint32_t Algorithm::getBestMax()
{
    return bestMax;
}

void Algorithm::setBestMax(uint32_t max)
{
    bestMax = max;
}

void Algorithm::compute()
{
#ifdef DEBUG
    std::cout << myID() << "Compute" << std::endl;
#endif

    if (matrix.getSize() < 1)
    {
        std::cerr << myID() << "ERROR: Matrix is empty!" << std::endl;
        exit(1);
    }

    uint32_t tmp = 0;

    std::vector<uint32_t> current;

#ifdef DEBUG
    /////////////////////////////////// TODO: Remove this debug

    std::chrono::milliseconds dura(5);
    std::this_thread::sleep_for(dura);

    ////////////////////////////////////
#endif

    int loopCycles = 10;

    while(!states.empty())
    {
        current = states.top();
        states.pop();

        uint32_t item = current.back();

        for(auto neigh : matrix.getNeigh(item))
        {
            uint32_t n = neigh;
            if(std::find(current.begin(),current.end(),n)==current.end())
            {
                current.push_back(neigh);
                states.push(current);
                current.pop_back();
            }
        }

        tmp = matrix.sum(current);
        if(tmp>bestMax)
        {
            bestMax = tmp;
        }
        if(loopCycles-- == 0)
        {
            return;
        }
    }
}
