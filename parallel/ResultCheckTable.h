#include <cstdint>

#ifndef RESULTCHECKTABLE_H
#define RESULTCHECKTABLE_H

class ResultCheckTable;

class ResultCheckTable
{
protected:
    uint8_t *checked;

    enum State
    {
        NO_RESPONSE,
        NO_JOB,
        HAS_JOB
    };
public:
    ResultCheckTable();
    ~ResultCheckTable();
    void init();
    void clear();
    void postResult(uint32_t id, bool hasJob);
    bool allResponded();
    bool isEnd();
};

#endif