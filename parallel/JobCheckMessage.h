#ifndef JOB_CHECK_MESSAGE
#define JOB_CHECK_MESSAGE

class JobCheckMessage;

#include "Message.h"

#define COLOR_WHITE 0
#define COLOR_BLACK 1

class JobCheckMessage : public Message
{
protected:
    uint8_t color;
    virtual void serializeRest(std::vector<uint8_t> &blob);
    virtual void unserializeRest(std::vector<uint8_t> &blob, uint32_t pos);
public:
    JobCheckMessage();
    JobCheckMessage(const uint8_t &color);
    virtual ~JobCheckMessage();
    uint8_t &getColor();
};


#endif
