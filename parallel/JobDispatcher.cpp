#include "JobDispatcher.h"
#include "MPIUtil.h"
#include "Common.h"

JobDispatcher::JobDispatcher()
    : nextRequest(0), lastBegin(0), acknowledged(false), started(false)
{

}

void JobDispatcher::updateInit()
{
    lastBegin = MPIUtil::id;
    nextRequest = (MPIUtil::id + 1) % MPIUtil::cpuCount;
}

uint32_t JobDispatcher::getNext()
{
    uint32_t res = nextRequest;
    nextRequest = (nextRequest + 1) % MPIUtil::cpuCount;
    return res;
}

bool JobDispatcher::hasStarted()
{
    return started;
}

void JobDispatcher::setStarted()
{
    started = true;
}

void JobDispatcher::resetStarted()
{
    lastBegin = nextRequest;
    started = false;
}

bool JobDispatcher::hasCheckedAll()
{
    return nextRequest == lastBegin;
}

void JobDispatcher::setAcknowledged()
{
    acknowledged = true;
}

void JobDispatcher::resetAcknowledged()
{
    acknowledged = false;
}

bool JobDispatcher::isAcknowledged()
{
    return acknowledged;
}

bool JobDispatcher::isAvailable()
{
    return available;
}

void JobDispatcher::setAvailable(bool available)
{
    this->available = available;
}

void JobDispatcher::updateLast()
{
    lastBegin = nextRequest;
    getNext();
}

uint32_t JobDispatcher::__get_nextRequest()
{
    return nextRequest;
}

uint32_t JobDispatcher::__get_lastBegin()
{
    return lastBegin;
}