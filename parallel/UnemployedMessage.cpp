#include "UnemployedMessage.h"

UnemployedMessage::UnemployedMessage()
    : Message(UNEMPLOYED_MESSAGE)
{

}
UnemployedMessage::~UnemployedMessage()
{
}

void UnemployedMessage::serializeRest(std::vector<uint8_t> &blob)
{
}

void UnemployedMessage::unserializeRest(std::vector<uint8_t> &blob, uint32_t pos)
{
}
