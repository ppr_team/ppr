#include "RequestMatrixMessage.h"

RequestMatrixMessage::RequestMatrixMessage()
    : Message(REQUESTMATRIX_MESSAGE)
{
}

RequestMatrixMessage::~RequestMatrixMessage()
{
}

void RequestMatrixMessage::serializeRest(std::vector<uint8_t> &blob)
{
}

void RequestMatrixMessage::unserializeRest(std::vector<uint8_t> &blob, uint32_t pos)
{
}
