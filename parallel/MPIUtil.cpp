#include "MPIUtil.h"
#include "Common.h"
#include "Comm.h"
#include <mpi/mpi.h>

namespace MPIUtil
{
    int32_t cpuCount;
    int32_t id;

    void initialize(int *argc, char ***argv)
    {
        std::cout << "Init MPI .." << std::endl;
        MPI_Init(argc, argv);
        MPI_Comm_rank(MPI_COMM_WORLD, &id);
        MPI_Comm_size(MPI_COMM_WORLD, &cpuCount);
        std::cout << "MPI initialized as id " << id << " from " << cpuCount << " cpus." << std::endl;
    }

    void sendBlocking(void *buffer, uint32_t size, uint32_t dest)
    {
        MPI_Send (buffer, size, MPI_UNSIGNED_CHAR, dest, 0, MPI_COMM_WORLD);
    }

    uint32_t receiveBlocking(void *buffer, uint32_t size)
    {
        int flag = 0;
        int length = 0;

#ifdef ASYNCHRONOUS_RECEIVER
        while(comm.receiver.isRunning())
#else
        while(comm.synchronousReceiver.isRunning())
#endif
        {
            MPI_Status status2;
            MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status2);
            if(flag)
            {
                MPI_Status status;
                MPI_Recv(buffer, size, MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                MPI_Get_count(&status, MPI_UNSIGNED_CHAR, &length);
                break;
            }
            std::chrono::milliseconds dura(1);
            std::this_thread::sleep_for(dura);
        }

        std::cout.flush();
        return (uint32_t) length;
    }

    uint32_t receiveImmediate(void *buffer, uint32_t size)
    {
        int length = 0;
        MPI_Status status;
        MPI_Recv(buffer, size, MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_UNSIGNED_CHAR, &length);
        return (uint32_t) length;
    }

    bool checkIncomming()
    {
        int flag = 0;
        MPI_Status status2;
        MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status2);
        return !!flag;
    }

    void finalize()
    {
        std::cout << myID() << "Finelize MPI .." << std::endl;
        MPI_Finalize();
        std::cout << myID() << "MPI finalized." << std::endl;
    }
}
