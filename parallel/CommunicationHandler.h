#ifndef COMMUNICATIONHANDLER_H
#define COMMUNICATIONHANDLER_H

class CommunicationHandler;

#include "FinishMessage.h"
#include "UnemployedMessage.h"
#include "NoJobAvailableMessage.h"
#include "JobAssignmentMessage.h"
#include "RequestMatrixMessage.h"
#include "MatrixUpdateMessage.h"
#include "ResultMessage.h"
#include "ExitMessage.h"
#include "JobCheckMessage.h"

class CommunicationHandler
{
protected:
    enum State
    {
        INIT,
        AWAIT_MATRIX,
        UNEMPLOYED,
        EMPLOYED,
        FINISH,
        NO_JOB,
        WAIT_FOR_FINISH,
        WAIT_FOR_EXIT,
        EXIT
    };

    State state;

    bool motherShipShouldSendJobCheck;
    bool hasJobCheck;
    uint8_t jobCheckColor;
    uint8_t myColor;

    void handleReceivedMessages();

    bool handleFinishMessage(FinishMessage *finishMessage);
    bool handleUnemployedMessage(UnemployedMessage *unemployedMessage);
    bool handleNoJobAvailableMessage(NoJobAvailableMessage *noJobAvailableMessage);
    bool handleJobAssignmentMessage(JobAssignmentMessage *jobAssignmentMessage);
    bool handleRequestMatrixMessage(RequestMatrixMessage *requestMatrixMessage);
    bool handleMatrixUpdateMessage(MatrixUpdateMessage *matrixUpdateMessage);
    bool handleResultMessage(ResultMessage *resultMessage);
    bool handleJobCheckMessage(JobCheckMessage *jobCheckMessage);
    bool handleExitMessage(ExitMessage *exitMessage);

    void handleInit();
    void handleAwaitMatrix();
    void handleUnemployed();
    void handleEmployed();
    void handleFinish();
    void handleNoJob();
    void handleWaitForFinish();
    void handleWaitForExit();
    void handleExit();
public:
    CommunicationHandler();
    void handle();
    bool shouldExit();
};

#endif
