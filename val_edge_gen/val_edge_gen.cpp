#include <iostream>
#include <cstdlib>
#include <ctime>

// praise the satan
int **mat = NULL;

int readVertCount()
{
	int val = 0;
	std::cin >> val;
	std::cin.get();
	return val;
}

int readEdge()
{
	int val = 0;
	val = std::cin.get();
	
	return val;
}

int genEdgeVal(int i, int j)
{
	if(i < j)
	{
		mat[i][j] = rand() & 0xFF;
		return mat[i][j];
	}
	else
	{
		if(i == j)
		{
			return 0;
		}
	}
	return mat[j][i];
}

void initRand()
{
	srand(time(NULL));
}

int main()
{
	initRand();

	int verts = readVertCount();
	std::cout << verts << std::endl;

	mat = new int *[verts];
	for(int i = 0; i < verts; i++)
	{
		mat[i] = new int[verts];
	}
	
	for(int i = 0; i < verts; i++)
	{
		for(int j = 0; j < verts; j++)
		{
			int edge = readEdge();
			
			if(edge == '1')
			{
				edge = genEdgeVal(i, j);
			}
			else
			{
				edge = 0;
			}
			
			std::cout << edge << " ";
		}
		std::cin.get();
		std::cout << std::endl;
	}
	
	for(int i = 0; i < verts; i++)
	{
		delete [] mat[i];
	}
	
	delete [] mat;
	
	return 0;

}

