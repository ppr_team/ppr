#include "Matrix.h"
#include <iostream>
#include <algorithm>

Matrix::Matrix(const size_t &size)
        : size(size)
{
    array = new uint8_t *[size];
    for (size_t i = 0; i < size; i++)
    {
        array[i] = new uint8_t[size];
    }
}

uint8_t &Matrix::get(size_t x, size_t y)
{
    return array[x][y];
}

uint8_t Matrix::get(size_t x, size_t y) const
{
    return array[x][y];
}

void Matrix::set(size_t x, size_t y, uint8_t val)
{
    array[x][y] = val;
}

void Matrix::fromStream(std::istream &is)
{
    for (size_t i = 0; i < size; i++)
    {
        for (size_t j = 0; j < size; j++)
        {
            int val;
            is >> val;
            array[i][j] = (uint8_t) val;
        }
    }
    std::cout << "Loaded matrix of side " << size << std::endl;

    /*for (size_t i = 0; i < size; i++)
    {
        for (size_t j = 0; j < size; j++)
        {
            std::cout<<array[i][j]<<" ";
        }
        std::cout<<std::endl;
    }*/
}

uint32_t Matrix::sum(std::vector<uint32_t> &vertSet) const
{
    uint32_t mySum = 0;
    uint32_t i = 0;
#ifdef debug
    std::cout << " computing set:";
    for (auto t : vertSet)
    {
        std::cout << t << " ";
    }
    std::cout << std::endl;
    std::cin.get();
#endif
    for (auto t : vertSet)
    {
        for (size_t j = 0; j < size; j++)
        {
            if (std::find(vertSet.begin(), vertSet.end(), j) == vertSet.end())
            {
                mySum += array[t][j];

                #ifdef debug
                std::cout << "mySum:" << mySum;
                std::cout << std::endl;
                std::cin.get();
                #endif
            }
        }
        i++;
    }
    //std::cout << std::endl;

    return mySum;
}

std::set<uint32_t> Matrix::getNeigh(uint32_t idx) const
{
    std::set<uint32_t> ret;

    for(int i = 0;i<size;i++)
    {
        if(array[idx][i]>0)
        {
            ret.insert(i);
        }
    }

    return ret;

}

const size_t &Matrix::getSize() const
{
    return size;
}
