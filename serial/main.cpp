#include <iostream>
#include <fstream>
#include "Matrix.h"
#include "Algorithm.h"

using namespace std;

int main(int argc, char **argv)
{
    size_t vertexCount = 0;
    bool fromFile = false;

    std::ifstream *ifs = new std::ifstream;

    if (argc == 2)
    {
        fromFile = true;
        ifs->open(argv[1]);

        *ifs >> vertexCount;
    }
    else
    {
        if (argc > 2)
        {
            std::cerr << "Invalid amount of arguments." << std::endl;
        }
        else
        {
            std::cin >> vertexCount;
        }
    }

    Matrix m(vertexCount);

    if (fromFile)
    {
        std::cout << "Reading from file." << std::endl;

        m.fromStream(*ifs);
        ifs->close();
    }
    else
    {
        m.fromStream(std::cin);
    }

    std::cout << "loaded." << std::endl;
    std::cout << "-> Computing." << std::endl;

    Algorithm algo;
    uint32_t max = algo.compute(m);

    std::cout << "The discovered max is: " << max << std::endl;

    delete ifs;

    return 0;
}

