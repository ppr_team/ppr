#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "Matrix.h"

/**
* The computing algorithm.
*
* This might as well be an abstract, although the project will
* benefit if it is split into 2 projects.
*/
class Algorithm
{
public:
    /**
    * The entry method for computation, will retrieve the value computed.
    */
    uint32_t compute(const Matrix &matrix);
};

#endif
