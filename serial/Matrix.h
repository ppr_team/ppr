#ifndef MATRIX_H
#define MATRIX_H

#include <cstdint>
#include <set>
#include <istream>
#include <vector>

class Matrix
{
private:
    size_t size;
    uint8_t **array;

public:

    Matrix(const size_t &size);

    uint8_t &get(size_t x, size_t y);

    uint8_t get(size_t x, size_t y) const;

    void set(size_t x, size_t y, uint8_t val);

    void fromStream(std::istream &is);

    uint32_t sum(std::vector<uint32_t> &set) const;

    std::set<uint32_t> getNeigh(uint32_t idx) const;

    const size_t &getSize() const;
};

#endif // MATRIX_H
