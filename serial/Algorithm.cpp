#include "Algorithm.h"
#include <stack>
#include <set>
#include <iostream>
#include <algorithm>
using namespace std;

//#define debug;

uint32_t Algorithm::compute(const Matrix &matrix)
{
    // need to check this, we need at least one vertex
    if (matrix.getSize() < 1)
    {
        return 0;
    }

    // The best maximum found
    uint32_t bestMax = 0;
    uint32_t tmp = 0;
    int32_t iterations = 0;

    
    stack<vector<uint32_t> > states;

    vector<uint32_t> current;
    //init stack
    for(uint32_t i = 0;i<matrix.getSize();i++)
    {
        vector<uint32_t> nodes;
        nodes.push_back(i);
        states.push(nodes);

#ifdef debug
        std::cout << " add set:";
        for (auto t : nodes)
        {
            std::cout << t << " ";
        }
        std::cout << std::endl;
        std::cin.get();
#endif
        nodes.clear();
    }

    while(!states.empty())
    {
        current = states.top();
        states.pop();

        uint32_t item = current.back();

            for(auto neigh : matrix.getNeigh(item))
            {
                uint32_t n = neigh;
                if(std::find(current.begin(),current.end(),n)==current.end())
                {
                    current.push_back(neigh);
                    states.push(current);
#ifdef debug
                    std::cout << " add set:";
                    for (auto t : current)
                    {
                        std::cout << t << " ";
                    }
                    std::cout << std::endl;
                    std::cin.get();
#endif
                    current.pop_back();
                }
            }


        tmp = matrix.sum(current);
        if(tmp>bestMax)
        {
            bestMax = tmp;
            #ifdef debug
            std::cout<<"bestMax:"<<bestMax<<std::endl;
            std::cout << "set:";
            for (auto t : current)
            {
                std::cout << t << " ";
            }
            std::cout << std::endl;
            #endif
        }

        iterations++;

    }

    std::cout << "The algorithm did " << iterations << " iterations." << std::endl;

    return bestMax;
}
